################
##linux
CXX          = nvcc
RM           = rm -f
AR           = ar cru

EXENAME		= timingTest
SRCEXT   	= cu
SRCDIR  	= src
INCDIR   	= inc
OBJDIR   	= build
EXEDIR  	= bin
SRCS    	:= $(shell find $(SRCDIR) -name '*.$(SRCEXT)')
OBJS    	:= $(patsubst $(SRCDIR)/%.$(SRCEXT),$(OBJDIR)/%.o,$(SRCS))

GARBAGE  = $(OBJDIR)/*.o $(EXEDIR)/$(EXENAME)

CXXFLAGS = -I$(INCDIR) #-res-usage -O3 --std=c++11

LINKFLAGS = -gencode arch=compute_75,code=sm_75

##Targets
all : $(EXEDIR)/$(EXENAME)

$(EXEDIR)/$(EXENAME) : $(OBJS)
	$(CXX) -o $@ $(OBJS) $(LINKFLAGS) $(LIBS)

$(OBJDIR)/%.o : $(SRCDIR)/%.$(SRCEXT)
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean   :
	$(RM) $(GARBAGE)

cleanall:
	$(RM) $(GARBAGE)
