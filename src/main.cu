#include "timingKernel.h"

#include <iostream>

#include <cuda_runtime.h>


// Entry point
int main( int argc, char * argv[] )
{
  // Access the GPU
  int devID = 0;
  cudaDeviceProp deviceProp;
  cudaSetDevice( devID );
  cudaGetDeviceProperties( &deviceProp, devID );
  std::cout << "GPU Device " << devID << ": " << deviceProp.name << " with compute capability " << deviceProp.major << "." << deviceProp.minor << std::endl;
  std::cout << "GPU shader clock " << deviceProp.clockRate << std::endl;

  clock_t const startTime = clock();
  TimingKernel::LaunchKernels();
  long double timeElapsed = (long double)( clock() - startTime );

  std::cout << "Ran test in " << timeElapsed/1E6 << "s" << std::endl;
  std::cout << "Last CUDA error message: " << cudaGetErrorString( cudaGetLastError() ) << std::endl;
}
