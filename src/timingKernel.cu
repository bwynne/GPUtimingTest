#include "timingKernel.h"

#include <iostream>

#include <cuda_runtime.h>

// Check if a sector has enough hits, and find the transformed parameters
__global__ static void TimingTest()
{
  clock_t start_time = clock();

  // Identify the thread
  uint16_t const threadX = threadIdx.x;
  uint16_t const threadY = threadIdx.y;
  uint16_t const blockX = blockIdx.x;

  clock_t stop_time = clock();

  printf( "Time from thread %u, %u, %u = %d\n", threadX, threadY, blockX, (int)(stop_time-start_time) );
}

void TimingKernel::LaunchKernels()
{
  dim3 grid( 1, 1 );
  dim3 threads( 32, 32 );
  TimingTest<<< grid, threads >>>();
  cudaDeviceSynchronize();
}
